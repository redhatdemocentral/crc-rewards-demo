#!/bin/sh 
DEMO="Red Hat Process Automation HR Rewards Install"
AUTHORS="Andrew Block, Eric D. Schabell"
PROJECT="git@gitlab.com:redhatdemocentral/crc-rewards-demo.git"
SUPPORT_DIR=./support
OC_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.9.10/"

# Adjust these variables to point to an OCP instance.
OPENSHIFT_ADMIN_PASS=
OPENSHIFT_ADMIN=kubeadmin
OPENSHIFT_USER=developer
OPENSHIFT_PWD=developer
HOST_IP=api.crc.testing   # set with OCP instance hostname or IP.
HOST_APPS=apps-crc.testing
HOST_PORT=6443
OCP_PRJ=appdev-in-cloud
OCP_APP=rewards

# rhpam container configuraiton.
KIE_ADMIN_USER=erics
KIE_ADMIN_PWD=redhatpam1!

# rewards project details.
PRJ_ID=rewards
PRJ_REPO="https://gitlab.com/bpmworkshop/rhpam-rewards-repo.git"
DELAY=540   # waiting max 8 min various container functions to startup.

# import container functions.
source support/container-functions.sh

# wipe screen.
clear 

echo
echo "###################################################################"
echo "##                                                               ##"   
echo "##  Setting up the ${DEMO} ##"
echo "##                                                               ##"   
echo "##             ####  ##### ####     #   #  ###  #####            ##"
echo "##             #   # #     #   #    #   # #   #   #              ##"
echo "##             ####  ###   #   #    ##### #####   #              ##"
echo "##             #  #  #     #   #    #   # #   #   #              ##"
echo "##             #   # ##### ####     #   # #   #   #              ##"
echo "##                                                               ##"
echo "##           ####  ####   ###   #### #####  ####  ####           ##"
echo "##           #   # #   # #   # #     #     #     #               ##"
echo "##           ####  ####  #   # #     ###    ###   ###            ##"
echo "##           #     #  #  #   # #     #         #     #           ##"
echo "##           #     #   #  ###   #### ##### ####  ####            ##"
echo "##                                                               ##"
echo "##   ###  #   # #####  ###  #   #  ###  ##### #####  ###  #   #  ##"
echo "##  #   # #   #   #   #   # ## ## #   #   #     #   #   # ##  #  ##"
echo "##  ##### #   #   #   #   # # # # #####   #     #   #   # # # #  ##"
echo "##  #   # #   #   #   #   # #   # #   #   #     #   #   # #  ##  ##"
echo "##  #   # #####   #    ###  #   # #   #   #   #####  ###  #   #  ##"
echo "##                                                               ##"
echo "##           #   #  ###  #   #  ###  ##### ##### ####            ##"
echo "##           ## ## #   # ##  # #   # #     #     #   #           ##"
echo "##           # # # ##### # # # ##### #  ## ###   ####            ##"
echo "##           #   # #   # #  ## #   # #   # #     #  #            ##"
echo "##           #   # #   # #   # #   # ##### ##### #   #           ##"
echo "##                                                               ##" 
echo "##                 #### #      ###  #   # ####                   ##"
echo "##            #   #     #     #   # #   # #   #                  ##"
echo "##           ###  #     #     #   # #   # #   #                  ##"
echo "##            #   #     #     #   # #   # #   #                  ##"
echo "##                 #### #####  ###   ###  ####                   ##"
echo "##                                                               ##"   
echo "##  brought to you by,                                           ##"   
echo "##             ${AUTHORS}                    ##"
echo "##                                                               ##"   
echo "##  ${PROJECT}        ##"
echo "##                                                               ##"   
echo "###################################################################"
echo

# check for passed target IP.
if [ $# -eq 1 ]; then
	echo "Checking for host ip passed as command line variable."
	echo
	if valid_ip "$1" || [ "$1" == "$HOST_IP" ]; then
		echo "OpenShift host given is a valid IP..."
		HOST_IP=$1
		echo
		echo "Proceeding with OpenShift host: $HOST_IP..."
		echo
	else
		# bad argument passed.
		echo "Please provide a valid IP that points to an OpenShift installation..."
		echo
		print_docs
		echo
		exit
	fi
elif [ $# -gt 1 ]; then
	print_docs
	echo
	exit
elif [ $# -eq 0 ]; then
	# validate HOST_IP.
  if [ -z ${HOST_IP} ]; then
	  # no host name set yet.
	  echo "No host name set in HOST_IP..."
	  echo
		print_docs
		echo
		exit
	else
		# host ip set, echo and proceed with hostname.
		echo "You've manually set HOST to '${HOST_IP}' so we'll use that for your OpenShift Container Platform target."
		echo
	fi
fi

# make some checks first before proceeding. 
command -v oc version --client >/dev/null 2>&1 || { echo >&2 "OpenShift CLI tooling is required but not installed yet... download here (unzip and put on your path): ${OC_URL}"; exit 1; }

echo "OpenShift commandline tooling is installed..."
echo 
echo "Checking if the admin password set to access openshift cluster..."
echo
if [ -z ${OPENSHIFT_ADMIN_PASS} ]; then
	echo "You need to update the variable OPENSHIFT_ADMIN_PASS with a valide admin password"
	echo "to login to your OpenShift cluster. If using CodeReady Containers, it was provided"
	echo "in the start up console. This variable is found at the top of the init script."
	echo "After updating, please run this init script again."
	echo
	exit
fi

echo "Logging in to OpenShift as ${OPENSHIFT_USER}..."
echo
oc login ${HOST_IP}:${HOST_PORT} --password=${OPENSHIFT_PWD} --username=${OPENSHIFT_USER}

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc login' command!"
	exit
fi

echo "Creating a new project..."
echo
oc new-project ${OCP_PRJ}

echo "Logging in to OpenShift as $OPENSHIFT_ADMIN..."
echo
oc login ${HOST_IP}:${HOST_PORT} --password=${OPENSHIFT_ADMIN_PASS} --username=${OPENSHIFT_ADMIN}

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc login' command!"
	exit
fi

echo
echo "Creating operator group for the businessautomation operator installation..."
echo
oc apply -f ${SUPPORT_DIR}/create-operatorgroup.yaml

if [ "$?" -ne "0" ]; then
	echo
  echo "Error occurred during 'oc apply' create operator group command!"
  exit
fi

echo
echo "Setting subscription information for the businessautomation operator installation..."
echo
oc apply -f ${SUPPORT_DIR}/sub-operator.yaml

if [ "$?" -ne "0" ]; then
	echo
  echo "Error occurred during 'oc apply' subscription command!"
  exit
fi

echo
echo "Logging in to OpenShift as ${OPENSHIFT_USER}..."
echo
oc login ${HOST_IP}:${HOST_PORT} --password=${OPENSHIFT_PWD} --username=${OPENSHIFT_USER}

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc login' command!"
	exit
fi

if instance_ready; then
	echo
	echo "The Red Hat Process Automation Manager instance has started using the operator..."
	echo
else
	echo
	echo "Exiting now with CodeReady Container started, but business automation operator"
	echo "did not finish installing an instance inside of the set time. Can't complete"
	echo "the installation of an instance for this project."
	echo
	exit
fi

if route_ready "rhpamcentr"; then
	echo "The business central insecure route has been added..."
	echo
else
	echo
	echo "Exiting now with CodeReady Container started, but insecure route was not added"
	echo "for decision central so can't complete the demo setup for this project."
	echo
	exit
fi

if route_ready "kieserver"; then
	echo "The kieserver insecure route has been added..."
	echo
else
	echo
	echo "Exiting now with CodeReady Container started, but insecure route was not added"
	echo "for kieserver so can't complete the demo setup for this project."
	echo
	exit
fi

if container_ready; then
	echo
	echo "The container has started..."
	echo
else
	echo "Exiting now but not sure if business central authoring environment is ready"
	echo "and did not install the demo project."
	echo
	exit
fi

if create_project_space; then
  echo "Creation of new space for project import succesful..."
  echo
else
  echo "Exiting now but not sure if project space is ready and did not install"
  echo "the demo project."
	echo 
	exit
fi

if validate_project_space; then
	echo "Creation of space successfully validated..."
	echo
else
	echo "Exiting now but not sure the project space is ready so can't import the"
	echo "demo project."
	echo
	exit
fi

if project_exists; then
	echo "Demo project already exists..."
	echo 
else
	echo "Project does not exist, importing in to container..."
	echo
	
	if project_imported; then
		echo "Imported project successfully..."
		echo
	else
	  echo "Exiting now but unable to import the project to business central and did"
	  echo "not install the demo project."
	  echo
		exit
	fi
fi

echo "================================================================================="
echo "=                                                                               ="
echo "=  Login to Red Hat Process Automation Manager to start exploring the HR        ="
echo "=  rewards project at:                                                          ="
echo "=                                                                               ="
echo "=   http://insecure-${OCP_APP}-rhpamcentr-${OCP_PRJ}.${HOST_APPS}        ="
echo "=                                                                               ="
echo "=    Log in: [ u:erics / p:redhatpam1! ]                                        ="
echo "=                                                                               ="
echo "=    Others:                                                                    ="
echo "=            [ u:kieserver / p:redhatpam1! ]                                    ="
echo "=            [ u:caseuser / p:redhatpam1! ]                                     ="
echo "=            [ u:casemanager / p:redhatpam1! ]                                  ="
echo "=            [ u:casesupplier / p:redhatpam1! ]                                 ="
echo "=                                                                               ="
echo "=  Explore the KieServer API docs here:                                         ="
echo "=                                                                               ="
echo "=   http://insecure-${OCP_APP}-kieserver-${OCP_PRJ}.${HOST_APPS}/docs    ="
echo "=                                                                               ="
echo "================================================================================="
echo

